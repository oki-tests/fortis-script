# fortis-challenge

The script is written in Python and utilizes ASDF and Poetry for dependency management.

## Installation Steps

- Install Python

`asdf install python 3.12.0`

- Install Poetry

`curl -sSL https://install.python-poetry.org | python3 -`

- Install Dependencies

`poetry install`

- Run Script

`poetry run python fortis/main.py`


## How to test

For testing the functionalities, a localstack environment is utilized.

- Install localstack

`brew install localstack/tap/localstack-cli`

- Start localstack

`localstack start`

- Test the script

`poetry run python tests/list_amis.py`


## Well-Known limitations

- Performance Enhancement: To enhance performance in large environments, pagination from boto3 is employed. The support for concurrency using the async library, called aiboto3, is limited to select services and does not include EC2. Additionally, adding concurrency could potentially trigger AWS rate limits.

- Boto3 returns a string `None`, so we had to convert to None type.
