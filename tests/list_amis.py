import unittest
import localstack_client.session as boto3
import json

from fortis.main import get_images_with_instances, add_info_to_images

class TestListAmis(unittest.TestCase):

    def setUp(self):
        self.client = boto3.client('ec2')

    def test_no_instances_should_return_empty(self):
        images = get_images_with_instances(self.client)
        output = add_info_to_images(self.client, images)
        self.assertEqual(output, {})

    def test_image_without_location_should_add_none(self):
        # AMI without location https://github.com/localstack/moto/blob/2a52ef3af05ce2449cea0c5d01edb2cf6cd5533d/moto/ec2/resources/latest_amis/us-east-1.json#L4
        ami = "ami-07c4798e349e3c74c"

        response = self.client.run_instances(
            ImageId=ami,
            InstanceType='t3.nano',
            MaxCount=1,
            MinCount=1
        )
        self.assertEqual(add_info_to_images(self.client, {ami: {"InstanceIds": []}})[ami]['ImageLocation'], None)

    def test_create_instance_per_ami_should_return_all_amis(self):
        # AMI without location https://github.com/localstack/moto/blob/2a52ef3af05ce2449cea0c5d01edb2cf6cd5533d/moto/ec2/resources/latest_amis/us-east-1.json#L4
        # Create an instance for each image available
        for image in self.client.describe_images()["Images"]:
            self.client.run_instances(
                ImageId=image["ImageId"],
                InstanceType='t3.nano',
                MaxCount=1,
                MinCount=1
            )
        len_images = len(self.client.describe_images()["Images"])
        len_images_listed = len(get_images_with_instances(self.client))

        self.assertEqual(len_images, len_images_listed)

    def tearDown(self) -> None:
        paginator = self.client.get_paginator('describe_instances')
        for page in paginator.paginate():
            for reservation in page['Reservations']:
                for instance in reservation['Instances']:
                    instance_id = instance['InstanceId']
                    self.client.terminate_instances(InstanceIds=[instance_id])

if __name__ == '__main__':
    unittest.main()
