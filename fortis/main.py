import boto3
import json

from typing import Union

from collections import defaultdict

def get_images_with_instances(client) -> dict[str, dict[str,list[str]]]:
    paginator = client.get_paginator('describe_instances')

    images = defaultdict(lambda:{"InstanceIds": []})
    for page in paginator.paginate():
        for reserve in page["Reservations"]:
            for instance in reserve["Instances"]:
                if instance["State"]["Name"] != "terminated":
                    images[instance["ImageId"]]["InstanceIds"].append(instance["InstanceId"])
    return images

def convert_string_none(str) -> Union[None, str]:
    if str == "None":
        return None
    return str

def add_info_to_images(client, images) -> dict[str, dict [str, str] | dict [str,list[str]]]:
    paginator = client.get_paginator('describe_images')

    for page in paginator.paginate(ImageIds=list(images)):
        for image in page["Images"]:
            images.get(image.get("ImageId"), {})["ImageDescription"] = convert_string_none(image.get("Description"))
            images.get(image.get("ImageId"), {})["ImageName"] = convert_string_none(image.get("Name"))
            images.get(image.get("ImageId"), {})["ImageLocation"] = convert_string_none(image.get("ImageLocation"))
            images.get(image.get("ImageId"), {})["OwnerId"] = convert_string_none(image.get("OwnerId"))
    return images

def main():
    try:
        ec2_c = boto3.client('ec2')
        images = get_images_with_instances(ec2_c)

        # print json format
        print(json.dumps(add_info_to_images(ec2_c, images)))

    except Exception as e:
        print(f"Exception: {e}")

if __name__ == "__main__":
    main()
